import appLoader from './loaders';
(async function(){
	const port = process.env.PORT || 3000;
	const app = await appLoader({config: {}});
	app.listen(port, (err) => {
		if (err) {
			console.log('Error has just happened --> ', err);
			process.exit(1);
			return;
		}
		console.log(`
        ################################################
        🛡️ Server listening on port: ${port}
        ################################################`);
	});
})();


import express from 'express';

const app = express();

app.get('/', (request, response) => {
    response.send('Hello from Express!');
});

app.get('/about', (request, response) => {
    response.send('About page!');
});
